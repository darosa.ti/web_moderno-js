# Desenvolvimento Web Moderno com JavaScript! COMPLETO 2020 + Projetos!

### Descrição: 
- Domine Web, 14 Cursos + Projetos, Javascript, Angular, React, Vue, Node, HTML, CSS, jQuery, Bootstrap Webpack Gulp MySQL.

### Fundamentos:
- Organização;
- Comentário;
- Variáveis e Constantes;
- Tipagem Fraca;
- Tipos: Number, String, Boolean, Array e Object;
- Math;
- Template Strings;
- Null & Undefined;
- Array;
- Objeto;
- Null Undefined;
- Function;
- Hoisting;
- Função vs Objeto;
- Par Nome/Valor;
- Notação Ponto;
- Destructuring; e
- Operadores.

### Estruturas de Controle:
- If;
- If/Else;
- If/Else If;
- Switch;
- While;
- Do/While;
- For;
- For/In; e
- Break/Continue.

### Função:
- Parâmetros;
- this;
- bind;
- Arrow;
- Anônimas;
- Callback;
- Construtoras;
- Tipos de Declaração;
- Contexto Léxico;
- Closures;
- Factory;
- Classe vs Função Factory;
- IIFE;
- Call e Apply; e
- Exercícios.
